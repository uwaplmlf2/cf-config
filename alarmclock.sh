#!/bin/bash
#
# Use the SBC supervisory microcontroller to schedule a wakeup time. This script
# must be installed in /lib/systemd/system-shutdown/ where it will be run by
# the systemd-halt service.
#
# Usage:
#
#  - Write the wakeup time to /.wakeup (in any valid date(1) format)
#  - Run: systemctl halt
#
# See https://wiki.embeddedarm.com/wiki/TS-7553-V2#Sleep
#
WAKEUP=$(cat /.wakeup)

if [[ $1 = "halt" && -n $WAKEUP ]]; then
    now=$(date +%s)
    next=$(date -d "$WAKEUP" +%s)
    dt=$((next - now))
    # The root filesystem is mounted read-only at this point,
    # remount it and rename the wakeup file.
    mount -o remount,rw /
    mv /.wakeup /.last-wakeup-request
    # Save debugging info
    echo "$(date -d @$next) [sleeptime = $dt seconds]" > /.starttime
    mount -o remount,ro /
    # Signal the microcontroller to turn off the CPU and reapply
    # power in $dt seconds.
    /usr/local/bin/tsmicroctl --sleep $dt
fi
