#!/usr/bin/env bash

set -e
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)

VERSION="${vers#v}"

export VERSION
nfpm pkg --target cf-setup_${VERSION}_any.deb

[[ "$1" = "release" ]] && ./bbupload.sh -E ./.env cf-setup_${VERSION}_any.deb
