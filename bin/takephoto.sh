#!/usr/bin/env bash

: ${ARCHIVE=/data/ARCHIVE}
: ${OUTGOING=/data/OUTGOING}
: ${OUTBOX=/data/OUTBOX}
: ${CFGDIR=/usr/local/etc/ueye}

scales=()
crop=
exposure=
settings="camera.ini"
for arg; do
    echo "Photo arg: $arg" 1>&2
    case "$arg" in
        scale*)
            scales+=("${arg##*=}")
            ;;
        crop*)
            crop="${arg##*=}"
            ;;
        exposure*)
            exposure="${arg##*=}"
            ;;
        settings*)
            settings="${arg##*=}"
            ;;
    esac
done

tref=$(date +%s)
t=$(date -d @$tref -u +'%Y%m%d_%H%M%S')
base="$ARCHIVE/photo/${t}"
mkdir -p "$(dirname $base)" 2> /dev/null 1>&2

# Build up command-line arguments for snapshot.py
if [[ -f "$CFGDIR/$settings" ]]; then
    pargs=("--loadfile" $CFGDIR/$settings)
else
    pargs=("--load")
fi
[[ -n "$exposure" ]] && pargs+=("--exposure" $exposure)
if [[ -n "$crop" ]]; then
    photo="${base}_crop"
    pargs+=("--aoi" "$crop")
else
    photo="${base}_full"
fi
pargs+=("${photo}.png")

logfile="$OUTGOING/explog_${t}.jsn"
# Take the photo
/usr/local/bin/snapshot.py --save "$logfile" "${pargs[@]}"
[[ $? = "0" ]] || {
    exit $?
}

# Create JPEG thumbnails. The thumbnails will be added to the
# OUTBOX if the scale value ends with a 'q'.
for sc in "${scales[@]}"; do
    queue=
    if [[ "${sc:(-1):1}" = "q" ]]; then
        queue=1
        n="${#sc}"
        ((n--))
        scale="${sc:0:$n}"
    else
        scale="$sc"
    fi
    # Create a shortened filename using the conventions used
    # with the MLF2 cameras. This naming format is expected
    # by the shore-side data archiving system.
    if ((scale > 80)); then
        prefix="l"
    elif ((scale > 30)); then
        prefix="m"
    else
        prefix="s"
    fi
    thumbfile="${prefix}_${t}.jpg"

    if ((scale == 100)); then
        gm convert "${photo}.png" "$ARCHIVE/photo/$thumbfile"
    else
        gm convert -resize "${scale}%" "${photo}.png" "$ARCHIVE/photo/$thumbfile"
    fi

    # Set the correct date/time in the EXIF header
    jhead -mkexif "$ARCHIVE/photo/$thumbfile" 1>&2
    jhead -ts$(date -d @$tref +%Y:%m:%d-%H:%M:%S) "$ARCHIVE/photo/$thumbfile" 1>&2
    if [[ -n $queue ]]; then
        mv "$ARCHIVE/photo/$thumbfile" "$OUTBOX/"
        echo "$thumbfile"
    fi
done

exit 0
