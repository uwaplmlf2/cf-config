#!/usr/bin/env bash
#
# Collect altimeter data and run the surface check analysis.
#
: ${ARCHIVE=/data/ARCHIVE}
: ${OUTBOX=/data/OUTBOX}
: ${ALTDEV=/dev/ttymxc4}

tlimit="${1:-500s}"

t=$(date +%s)
workdir="a$(date -d @$t -u +%j%H%M)"
mkdir -p "$workdir"
cd $workdir

outfile="alt_$(date -d @$t -u +%Y%m%d_%H%M%S).nmea"
# Acquire altimeter data
lognmea --baud 38400 -o $outfile $ALTDEV $tlimit
# Run surface check analysis and save exit code
surfcheck $outfile
ec=$?
# Package the data and copy to the OUTBOX
cd ..
yd="$(date -d @$t -u +%Y/%j)"
mkdir -p $ARCHIVE/$yd
tar -c -z -f "$ARCHIVE/$yd/${workdir}.tgz" "$workdir"
ln "$ARCHIVE/$yd/${workdir}.tgz" "$OUTBOX/${workdir}.tgz"
rm -rf "$workdir"

exit $ec
