#!/usr/bin/env bash
#
# Wait for the Quicksilver Iridium modem to establish a link with
# the satellite constellation.
#

export PATH=/usr/local/bin:$PATH

ipaddr=$(ip -4 -o a show eth0 2>/dev/null | sed -e 's/.* inet \([^ ]*\).*/\1/')

# Wait for the modem to assign an IP address
while [[ -z $ipaddr ]]; do
    sleep 1
    ipaddr=$(ip -4 -o a show eth0 2>/dev/null | sed -e 's/.* inet \([^ ]*\).*/\1/')
done

echo "IP address assigned: $ipaddr"

# Wait for connection to the satellites
vis="$(qsctl info | jq -r '.stat.visible')"
while [[ $vis != "true" ]]; do
    sleep 1
    vis="$(qsctl info | jq -r '.stat.visible')"
done

echo "Satellite link established"
