#!/usr/bin/env python3
#
# Usage: snapshot.py [options] outfile
#
# Take a snapshot with the IDS camera and write the image to OUTFILE in
# PNG format.

from pyueye import ueye  # type: ignore
import ctypes
import sys
import argparse
import time
import json
from collections import OrderedDict
try:
    from typing import Tuple, Dict, Any, List, Iterable
except ImportError:
    pass


# Autoexposure timeout, seconds
AE_TIMEOUT = 45  # type: int


def geom_to_rect(geom: str) -> ueye.IS_RECT:
    """
    Create a Ueye rect object from a geometery string.
    """
    size, p = geom.split("+", 1)
    w, h = size.split("x")
    if p:
        x, y = p.split("+")
    else:
        x, y = "0", "0"
    rect = ueye.IS_RECT()
    rect.s32X = int(x)
    rect.s32Y = int(y)
    rect.s32Width = int(w)
    rect.s32Height = int(h)
    return rect


def cam_init():
    # type: () -> Tuple[ueye.c_uint, ueye.c_mem_p, ueye.c_int, ueye.c_int]
    """
    Allocate camera object and initialize.
    """
    hcam = ueye.HIDS(0)
    pccmem = ueye.c_mem_p()
    memID = ueye.c_int()
    hWnd = ctypes.c_void_p()
    nret = ueye.is_InitCamera(hcam, hWnd)
    return hcam, pccmem, memID, nret


def auto_mode(hcam: ueye.c_uint):
    """
    Enable auto-gain, auto-shutter, and auto framerate.
    """
    enable = ueye.DOUBLE(1)
    zero = ueye.DOUBLE(0)
    ueye.is_SetAutoParameter(hcam, ueye.IS_SET_ENABLE_AUTO_GAIN, enable, zero)
    ueye.is_SetAutoParameter(hcam, ueye.IS_SET_ENABLE_AUTO_SHUTTER, enable, zero)
    ueye.is_SetAutoParameter(hcam, ueye.IS_SET_ENABLE_AUTO_FRAMERATE, enable, zero)


def cam_close(hcam: ueye.c_uint, pccmem: ueye.c_mem_p, memID: ueye.c_int):
    """
    Free memory and close camera interface.
    """
    ueye.is_FreeImageMem(hcam, pccmem, memID)
    ueye.is_ExitCamera(hcam)


def capture(hcam: ueye.c_uint, filename: str) -> ueye.c_int:
    """
    Capture an image and save to a PNG file.
    """
    nret = ueye.is_FreezeVideo(hcam, ueye.IS_DONT_WAIT)
    if nret == ueye.IS_SUCCESS:
        fp = ueye.IMAGE_FILE_PARAMS()
        fp.pwchFileName = filename
        fp.nFileType = ueye.IS_IMG_PNG
        fp.ppcImageMem = None
        fp.pnImageID = None
        nret = ueye.is_ImageFile(hcam, ueye.IS_IMAGE_FILE_CMD_SAVE,
                                 fp, ueye.sizeof(fp))
    return nret


def capture_auto(hcam, filename, trace=False):
    # type: (ueye.c_uint, str, bool) -> Tuple[ueye.c_int, List[OrderedDict[str, Any]]]
    """
    Capture an image (with auto exposure & gain) and save to a PNG file.
    """
    ms = ueye.DOUBLE()
    pInfo = ueye.UEYE_AUTO_INFO()

    # Start the camera in freerun (video) mode:
    nret = ueye.is_CaptureVideo(hcam, ueye.IS_DONT_WAIT)
    if nret != ueye.IS_SUCCESS:
        print("Cannot start video capture ({nret})".format(nret=nret),
              file=sys.stderr)
        return nret

    result = []

    if not trace:
        print("Adjusting exposure ", end="", flush=True, file=sys.stderr)

    # AE should be running at this point.
    # We use "run once" mode, so AE is turned off when finished
    # We wait for (urCtrlStatus==ACS_DISABLED==4) or timeout
    # This is more stringent than waiting for ACS_FINISHED
    for k in range(AE_TIMEOUT):
        # Check the AE progress
        ueye.is_GetAutoInfo(hcam, pInfo)
        ueye.is_Exposure(hcam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE,
                         ms, ueye.sizeof(ms))
        gain = ueye.is_SetHWGainFactor(hcam, ueye.IS_GET_MASTER_GAIN_FACTOR,
                                       100)
        result.append(OrderedDict(exposure=ms.value, gain=gain,
                                  brightness=int(pInfo.sBrightCtrlStatus.curValue),
                                  controller=int(pInfo.sBrightCtrlStatus.curController),
                                  status=int(pInfo.sBrightCtrlStatus.curCtrlStatus)))
        if trace:
            print(ms.value,gain,pInfo.sBrightCtrlStatus.curValue,pInfo.sBrightCtrlStatus.curController,pInfo.sBrightCtrlStatus.curCtrlStatus, file=sys.stderr,sep=" ")
        else:
            print(".", end="", flush=True, file=sys.stderr)

        if pInfo.sBrightCtrlStatus.curCtrlStatus == ueye.ACS_DISABLED:
            break
        else:
            time.sleep(1)

    if not trace:
        print(" done", flush=True, file=sys.stderr)
    nret = ueye.is_StopLiveVideo(hcam, ueye.IS_DONT_WAIT)

    # Save the last image
    if nret == ueye.IS_SUCCESS:
        fp = ueye.IMAGE_FILE_PARAMS()
        fp.pwchFileName = filename
        fp.nFileType = ueye.IS_IMG_PNG
        fp.ppcImageMem = None
        fp.pnImageID = None
        nret = ueye.is_ImageFile(hcam, ueye.IS_IMAGE_FILE_CMD_SAVE, fp,
                                 ueye.sizeof(fp))

    return (nret, result)


def save_ndjson(vals, filename):
    # type: (Iterable[Dict[str, Any]], str) -> None
    """
    Save each element of vars as a line in an ndjson file.
    """
    with open(filename, mode="w") as f:
        for val in vals:
            print(json.dumps(val), file=f)


def main():
    parser = argparse.ArgumentParser(description="Capture an image from an IDS camera")
    parser.add_argument("outfile", type=str,
                        help="output file name")
    parser.add_argument("--aoi", type=str,
                        metavar="GEOM",
                        help="specify Area Of Interest as WIDTHxHEIGHT+X+Y")
    parser.add_argument("--load", action="store_true",
                        help="load settings from camera EEPROM")
    parser.add_argument("--loadfile", type=str,
                        metavar="FILENAME",
                        help="load settings from a file")
    parser.add_argument("--exposure", type=float,
                        metavar="MS",
                        help="set exposure time in milliseconds")
    parser.add_argument("--save", type=str,
                        metavar="FILENAME",
                        help="save auto-exposure results to FILENAME")
    parser.add_argument("--trace", action="store_true",
                        help="write auto-exposure status to stderr")
    args = parser.parse_args()

    hcam, pccmem, memID, nret = cam_init()
    if nret != ueye.IS_SUCCESS:
        print("Camera initialization failed ({nret})".format(nret=nret),
              file=sys.stderr)
        return 0
    sensorinfo = ueye.SENSORINFO()
    ueye.is_GetSensorInfo(hcam, sensorinfo)

    zero = ueye.UINT(0)
    if args.load:
        nret = ueye.is_ParameterSet(hcam, ueye.IS_PARAMETERSET_CMD_LOAD_EEPROM,
                                    zero, zero)
        if nret != ueye.IS_SUCCESS:
            print("Parameter EEPROM load failed ({nret})".format(nret=nret),
                  file=sys.stderr)
    elif args.loadfile:
        name = ctypes.c_wchar_p(args.loadfile)
        nret = ueye.is_ParameterSet(hcam, ueye.IS_PARAMETERSET_CMD_LOAD_FILE,
                                    name, zero)
        if nret != ueye.IS_SUCCESS:
            print("Parameter load failed ({nret})".format(nret=nret),
                  file=sys.stderr)
    else:
        auto_mode(hcam)

    if args.aoi:
        rect = geom_to_rect(args.aoi)
        nret = ueye.is_AOI(hcam, ueye.IS_AOI_IMAGE_SET_AOI, rect,
                           ctypes.sizeof(rect))
        if nret != ueye.IS_SUCCESS:
            print("Cannot set AOI ({nret})".format(nret=nret),
                  file=sys.stderr)
            ueye.is_AllocImageMem(hcam, sensorinfo.nMaxWidth,
                                  sensorinfo.nMaxHeight, 24, pccmem, memID)
        else:
            ueye.is_AllocImageMem(hcam, rect.s32Width,
                                  rect.s32Height, 24, pccmem, memID)
    else:
        ueye.is_AllocImageMem(hcam, sensorinfo.nMaxWidth,
                              sensorinfo.nMaxHeight, 24, pccmem, memID)

    ueye.is_SetImageMem(hcam, pccmem, memID)

    if args.exposure:
        # fixed exposure
        val = ueye.DOUBLE(args.exposure)
        ueye.is_Exposure(hcam, ueye.IS_EXPOSURE_CMD_SET_EXPOSURE, val,
                         ueye.sizeof(val))
        ms = ueye.DOUBLE()
        ueye.is_Exposure(hcam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE, ms,
                         ueye.sizeof(ms))
        print("Exposure: {ms.value:.1f}".format(ms=ms), file=sys.stderr)

        # Grab an image and write to a PNG file
        status = 0
        nret = capture(hcam, args.outfile)
        if nret != ueye.IS_SUCCESS:
            status = 1
            print("Image capture failed ({nret})".format(nret=nret),
                  file=sys.stderr)
    else:
        # Autoexposure
        # Grab an image and write to a PNG file
        status = 0
        nret, result = capture_auto(hcam, args.outfile, trace=args.trace)
        if nret != ueye.IS_SUCCESS:
            status = 1
            print("Image capture failed ({nret})".format(nret=nret),
                  file=sys.stderr)
        else:
            if args.save:
                save_ndjson(result, args.save)

    cam_close(hcam, pccmem, memID)
    return status


if __name__ == "__main__":
    sys.exit(main())
